import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'bookmark-edit',
  templateUrl: './bookmark-edit.component.html',
  styleUrls: ['./bookmark-edit.component.scss']
})

export class BookmarkEditComponent {

    @Input() bookmark = {};
    @Output() save = new EventEmitter();
    onSave() {
        this.save.emit(this.bookmark);
    }

}