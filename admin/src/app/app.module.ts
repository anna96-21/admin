import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http'
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { AdminComponent } from './admin/admin.component';
import {  BookmarkEditComponent }  from './admin/bookmark-edit/bookmark-edit.component';
import {  BookmarkListComponent }  from './admin/bookmark-list/bookmark-list.component';
import { BookmarkService } from './admin/bookmark.service';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    BookmarkEditComponent,
    BookmarkListComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [BookmarkService],
  bootstrap: [AppComponent]
})
export class AppModule { }
