import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  email: string;
  password: string;
  gForm: FormGroup;
  post: any;

  constructor(private fb: FormBuilder){
    this.gForm = fb.group({
      'email': ['', 
                  [ Validators.required, 
                    <any>Validators.pattern('^[a-zA-Z0-9_]+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}')]
                ],
      'password': ['', 
                    [ Validators.required, 
                      <any>Validators.minLength(5),
                    ]
                  ],
    })}

  submitUser(post){
    this.email = post.email;
    this.password = post.password;
  }
  



}
